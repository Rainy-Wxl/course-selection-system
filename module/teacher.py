class teacher:

    def __init__(self, name, salary):
        self.name = name
        self.salary = salary
        self.classes = {}  # 一个老师可以对应多个班级

    def teacher_add_classes(self, classes_id, classes):
        self.classes[classes_id] = classes  # {班id:班级实例}
